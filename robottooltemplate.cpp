#include "robottooltemplate.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(RobotToolTemplate, SkFlowSat)
{
    setObjectName("RobotToolTemplate");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotToolTemplate::onSetup()
{
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotToolTemplate::onInit()
{}

void RobotToolTemplate::onQuit()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotToolTemplate::onFastTick()
{}

void RobotToolTemplate::onSlowTick()
{}

void RobotToolTemplate::onOneSecTick()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
void RobotToolTemplate::onCheckInternals()
{}

void RobotToolTemplate::onTickParamsChanged()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotToolTemplate::onChannelAdded(SkFlowChanID chanID)
{}

void RobotToolTemplate::onChannelRemoved(SkFlowChanID chanID)
{}

void RobotToolTemplate::onChannelHeaderSetup(SkFlowChanID chanID)
{}

void RobotToolTemplate::onFlowDataCome(SkFlowChannelData &chData)
{}

void RobotToolTemplate::onChannelPublishStartRequest(SkFlowChanID chanID)
{}

void RobotToolTemplate::onChannelPublishStopRequest(SkFlowChanID chanID)
{}

void RobotToolTemplate::onChannelRedistrStartRequest(SkFlowChanID chanID)
{}

void RobotToolTemplate::onChannelRedistrStopRequest(SkFlowChanID chanID)
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
