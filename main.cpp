#include "robottooltemplate.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    //SkCli *cli = skApp->appCli();
    //cli->add("--i2c-device", "-d", "/dev/i2c-1", "Setup the i2c bus");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new RobotToolTemplate;

    return skApp->exec();
}
