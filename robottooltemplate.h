#ifndef ROBOTTOOLTEMPLATE_H
#define ROBOTTOOLTEMPLATE_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/FlowNetwork/skflowsat.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class RobotToolTemplate extends SkFlowSat
{
    public:
        Constructor(RobotToolTemplate, SkFlowSat);

    private:
        bool onSetup()                                              override;
        void onInit()                                               override;
        void onQuit()                                               override;

        void onFastTick()                                           override;
        void onSlowTick()                                           override;
        void onOneSecTick()                                         override;

        void onCheckInternals()                                     override;
        void onTickParamsChanged()                                  override;

        void onChannelAdded(SkFlowChanID chanID)                    override;
        void onChannelRemoved(SkFlowChanID chanID)                  override;
        void onChannelHeaderSetup(SkFlowChanID chanID)              override;

        void onFlowDataCome(SkFlowChannelData &chData)              override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)      override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)       override;

        void onChannelRedistrStartRequest(SkFlowChanID chanID)      override;
        void onChannelRedistrStopRequest(SkFlowChanID chanID)       override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ROBOTTOOLTEMPLATE_H
